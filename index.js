import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'

connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true })

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})

app.post('/users', (req, res) => {
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

//Hi there! The first task was: find user by id and update info, so I used "put request" and testings with postman showed it
// was updated successfully;
app.put('/users/:id', function (req, res){
  let id = req.params.id;
  UserModel.findOneAndUpdate(id, req.body, {new: true}, function (err, result) {
    if(err){
      res.send(err)
    }
    else{
      res.send("Updated:" + result)
    }
  });
});
//The second task was: find user by id and remove, so I used delete request and it works as well fine within postman
app.delete('/users/:id', function (req, res){
  UserModel.findByIdAndDelete(req.params.id, function (err, result) {
    if(err){
      res.send(err)
    }
    else{
      res.send("Deleted: " + result)
    }
  });
});

app.listen(8080, () => console.log('Example app listening on port 8080!'))
